#include <Keyboard.h>
#include <Bounce2.h>

#define LED_PIN 17

#define NUM_BUTTONS 4
const uint8_t BUTTON_PINS[NUM_BUTTONS] = {14, 8, 7, 15};
const char KEYS[NUM_BUTTONS] = {'w', 'a', 's', 'd'};

// Instantiate a Bounce object
Bounce *buttons = new Bounce[NUM_BUTTONS];

void setup()
{
  delay(2000);
  for (int i = 0; i < NUM_BUTTONS; i++)
  {
    buttons[i].attach(BUTTON_PINS[i], INPUT_PULLUP); //setup the bounce instance for the current button
    buttons[i].interval(0.6);                        // interval in ms
  }
  //Setup the LED :
  pinMode(LED_PIN, OUTPUT);

  Keyboard.begin();
}

void loop()
{

  for (int i = 0; i < NUM_BUTTONS; i++)
  {
    // Update the Bounce instance :
    buttons[i].update();
    // If low, send key pressed
    if (buttons[i].read() == LOW)
    {
      digitalWrite(LED_PIN, HIGH);
      Keyboard.press(KEYS[i]);
    }
    else
    { // Else, send key released
      digitalWrite(LED_PIN, LOW);
      Keyboard.release(KEYS[i]);
    }
  }
}
